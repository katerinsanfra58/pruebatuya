# PruebaTuya

#Prerequisitos
-Java versión superior a 8
-IntelliJ IDE o Eclipse IDE
-Gladle superior a v 5 (Variables de entorno configuradas)
-Git

#Para realizar la ejecución de las pruebas realizar los siguientes pasos.

#Por cmd:
-Ejecutar el comando:  gradlew clean test --tests AppiGeonameRunner


#Manual:
- Dirígete a la carpeta src/test/runner/AppiGeonameRunner, clic derecho ' Run AppiGeonameRunner 
- Puedes visualizar cómo corre la prueba.
